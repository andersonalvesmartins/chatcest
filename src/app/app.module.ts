import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireModule } from '@angular/fire'
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

const firebaseConfig = {
  apiKey: "AIzaSyAui72kuPG9B1mpcq8uhd9N7BKDBrcPP9U",
  authDomain: "chatapp-1571703005199.firebaseapp.com",
  databaseURL: "https://chatapp-1571703005199.firebaseio.com",
  projectId: "chatapp-1571703005199",
  storageBucket: "chatapp-1571703005199.appspot.com",
  messagingSenderId: "392298299437",
  appId: "1:392298299437:web:44fef98e4cf859010bb8a4",
  measurementId: "G-EM7Y6SGQC3"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AngularFireModule, AppRoutingModule,AngularFireModule.initializeApp(firebaseConfig)],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
